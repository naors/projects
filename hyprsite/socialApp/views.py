# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello, world. You're at the social app index.")


def scrap(request, user_name):
    response = "You're looking at the results of scrap user name %s. his image url is %s"
    return HttpResponse(response % (user_name,"test.url.test"))

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)